#import torch
import torch
from torch import nn
import torch.utils.data as td
import torch.optim as optim


import os
import time
import scipy
import scipy.misc
import numpy as np
import argparse
from abc import ABC, abstractmethod

# local import
from model import ResnetGenerator, Discriminator
from get_dataset import StyleDataset
import utils

class CycleGAN(ABC):

	def __init__(self, opts):
		super(CycleGAN, self).__init__()

		# Choose device
		self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

		# Give options
		self.opts = opts

		# Determine if use batch normalization or not
		self.opts.BatchNorm = self.opts.batch_size>1
		self.opts.use_bias = not self.opts.BatchNorm

		# Create models
		self.create_model()
		self.print_models()

		# Create data loader
		self.train_set_A = StyleDataset(self.opts, mode='train', image_type=self.opts.A)
		self.train_loader_A = td.DataLoader(self.train_set_A, batch_size=opts.batch_size, shuffle=True, pin_memory=True)

		self.train_set_B = StyleDataset(self.opts, mode='train', image_type=self.opts.B)
		self.train_loader_B = td.DataLoader(self.train_set_B, batch_size=self.opts.batch_size, shuffle=True, pin_memory=True)

		self.test_set_A = StyleDataset(self.opts, mode='test', image_type=self.opts.A)
		self.test_loader_A = td.DataLoader(self.test_set_A, batch_size=self.opts.num_sample, shuffle=False, pin_memory=True)

		self.test_set_B = StyleDataset(self.opts, mode='test', image_type=self.opts.B)
		self.test_loader_B = td.DataLoader(self.test_set_B, batch_size=self.opts.num_sample, shuffle=False, pin_memory=True)
	
		# Create optimizers for the generators and discriminators
		# Get generator parameters
		G_params = list(self.G_AtoB.parameters()) + list(self.G_BtoA.parameters())
		# Get discriminator parameters
		D_params = list(self.D_A.parameters()) + list(self.D_B.parameters()) 
		# Create optimizers
		self.G_optimizer = optim.Adam(G_params, self.opts.lr, [self.opts.beta1, self.opts.beta2])
		self.D_optimizer = optim.Adam(D_params, self.opts.lr, [self.opts.beta1, self.opts.beta2])

		# Define loss
		self.GANloss = nn.MSELoss()
		self.cycle_loss = nn.L1Loss()
		self.idt_loss = nn.L1Loss()

	def print_models(self):
		"""
		Description
    	===========
		Prints model information for the generators and discriminators.
		"""		
		print("                 G_AtoB                ")
		print("---------------------------------------")
		print(self.G_AtoB)
		print("---------------------------------------")
		print("                 G_BtoA                ")
		print("---------------------------------------")
		print(self.G_BtoA)
		print("---------------------------------------")
		print("                  D_A                  ")
		print("---------------------------------------")
		print(self.D_A)
		print("---------------------------------------")
		print("                  D_B                  ")
		print("---------------------------------------")
		print(self.D_B)
		print("---------------------------------------")

	def create_model(self):
		"""
		Description
    	===========
    	Create 2 generators and 2 discriminoators, and load model parameters if asked to.
		"""
		# Generatot, A to B
		self.G_AtoB = ResnetGenerator(self.opts.BatchNorm, self.opts.use_bias, self.opts.num_resblocks).to(self.device)
		# Generatot, B to A
		self.G_BtoA = ResnetGenerator(self.opts.BatchNorm, self.opts.use_bias, self.opts.num_resblocks).to(self.device)
		# Discriminator of A
		self.D_A = Discriminator(self.opts.BatchNorm, self.opts.use_bias).to(self.device)
		# Discriminator of B
		self.D_B = Discriminator(self.opts.BatchNorm, self.opts.use_bias).to(self.device)
		
		self.epoch_num = 1
		

		# If continue trainig, then load trained models
		if self.opts.load is not None:
			self.epoch_num = self.opts.epoch_num
			# Create paths
			G_AtoB_path = os.path.join(self.opts.load, 'G_AtoB_{}.pkl'.format(self.epoch_num))
			G_BtoA_path = os.path.join(self.opts.load, 'G_BtoA_{}.pkl'.format(self.epoch_num))
			D_A_path = os.path.join(self.opts.load, 'D_A_{}.pkl'.format(self.epoch_num))
			D_B_path = os.path.join(self.opts.load, 'D_B_{}.pkl'.format(self.epoch_num))
			# Load models
			self.G_AtoB.load_state_dict(torch.load(G_AtoB_path, map_location=lambda storage, loc: storage))
			self.G_BtoA.load_state_dict(torch.load(G_BtoA_path, map_location=lambda storage, loc: storage))
			self.D_A.load_state_dict(torch.load(D_A_path, map_location=lambda storage, loc: storage))
			self.D_B.load_state_dict(torch.load(D_B_path, map_location=lambda storage, loc: storage))
			self.epoch_num += 1


	def save_checkpoint(self):
		"""
		Description
    	===========
		Saves the parameters of both generators G_BtoA, G_AtoB and discriminators D_A, D_B.
		Checkpoints are saved at opts.checkpoint_dir
		"""
		G_AtoB_path = os.path.join(self.opts.checkpoint_dir, 'G_AtoB_{}.pkl'.format(self.epoch_num))
		G_BtoA_path = os.path.join(self.opts.checkpoint_dir, 'G_BtoA_{}.pkl'.format(self.epoch_num))
		D_A_path = os.path.join(self.opts.checkpoint_dir, 'D_A_{}.pkl'.format(self.epoch_num))
		D_B_path = os.path.join(self.opts.checkpoint_dir, 'D_B_{}.pkl'.format(self.epoch_num))
		torch.save(self.G_AtoB.state_dict(), G_AtoB_path)
		torch.save(self.G_BtoA.state_dict(), G_BtoA_path)
		torch.save(self.D_A.state_dict(), D_A_path)
		torch.save(self.D_B.state_dict(), D_B_path)
		


	def save_samples(self):
		"""
		Description
    	===========
		During traing, give some test images to two generators and save the results
		"""

		# get generated images
		fake_A = self.G_BtoA(self.fixed_B)
		fake_B = self.G_AtoB(self.fixed_A)

		# convert it from tensor to numpy
		A, fake_A = utils.to_data(self.fixed_A), utils.to_data(fake_A)
		B, fake_B = utils.to_data(self.fixed_B), utils.to_data(fake_B)

		# merge real image with generated image and save
		merged = merge_images( A, fake_B, self.opts)
		file_name = 'sample_' + self.opts.A + '_to_' + self.opts.B + '_{}.png'.format(self.epoch_num)
		path = os.path.join(self.opts.sample_dir, file_name)
		scipy.misc.imsave(path, merged)

		merged = merge_images(B, fake_A, self.opts)
		file_name = 'sample_' + self.opts.B + '_to_' + self.opts.A + '_{}.png'.format(self.epoch_num)
		path = os.path.join(self.opts.sample_dir, file_name)
		scipy.misc.imsave(path, merged)
		

	def train(self):
		"""
		Description
    	===========
		Start training
		"""

		# Get some fixed test images to evaluate two generators during training
		test_iter_A = iter(self.test_loader_A)
		test_iter_B = iter(self.test_loader_B)
		self.fixed_A = utils.to_var(test_iter_A.next())
		self.fixed_B = utils.to_var(test_iter_B.next())


		for epoch in range(self.opts.train_epochs):
			# Record time
			t_start = time.time()

			print('Start epoch {}'.format(self.epoch_num))

			# Load data
			iter_A = iter(self.train_loader_A)
			iter_B = iter(self.train_loader_B)

			# Number of iterations in one epoch
			iter_per_epoch = min(len(iter_A), len(iter_B))

			for iteration in range(iter_per_epoch):
			
				# Get training image
				images_A = iter_A.next()
				images_A = utils.to_var(images_A)
				images_B = iter_B.next()
				images_B = utils.to_var(images_B)

				# Train generators
				self.G_optimizer.zero_grad()
				set_requires_grad([self.D_A, self.D_B], False)
				# Compute losses of generator BtoA
				fake_A = self.G_BtoA.forward(images_B)
				pre_g_A = self.D_A.forward(fake_A)
				target = get_target_tensor(pre_g_A, True)
				G_loss_BtoA = self.GANloss(pre_g_A, target)
				# cycle loss
				reconstructed_B = self.G_AtoB(fake_A)
				cycle_loss_BtoAtoB = self.cycle_loss(reconstructed_B, images_B) * self.opts.lambda_B

				# Compute losses of generator AtoB
				fake_B = self.G_AtoB.forward(images_A)
				pre_g_B = self.D_B.forward(fake_B)
				G_loss_AtoB = self.GANloss(pre_g_B, get_target_tensor(pre_g_B, True))
				# cycle loss
				reconstructed_A = self.G_BtoA(fake_B)
				cycle_loss_AtoBtoA = self.cycle_loss(reconstructed_A, images_A) * self.opts.lambda_A

				if self.opts.use_identity_loss:
					idt_A = self.G_BtoA(images_A)
					idt_loss_A = self.idt_loss(idt_A, images_A)*self.opts.lambda_A*self.opts.lambda_idt
					idt_B = self.G_AtoB(images_B)
					idt_loss_B = self.idt_loss(idt_B, images_B)*self.opts.lambda_B*self.opts.lambda_idt

				# losses for generators
				G_loss = G_loss_AtoB + G_loss_BtoA + cycle_loss_AtoBtoA + cycle_loss_BtoAtoB + idt_loss_A + idt_loss_B
				G_loss.backward()
				self.G_optimizer.step()
				
				# Train discriminators
				self.D_optimizer.zero_grad()
				set_requires_grad([self.D_A, self.D_B], True)
				# Compute the losses for D_A
				pre_real_A = self.D_A.forward(images_A)
				D_A_loss_real = self.GANloss(pre_real_A, get_target_tensor(pre_real_A, True))
				fake_A = self.G_BtoA.forward(images_B)
				pre_fake_A = self.D_A.forward(fake_A)
				D_A_loss_fake = self.GANloss(pre_fake_A, get_target_tensor(pre_fake_A, False))
				D_A_loss = (D_A_loss_real + D_A_loss_fake) * 0.5
				# Compute the losses for D_B
				pre_real_B = self.D_B.forward(images_B)
				D_B_loss_real = self.GANloss(pre_real_B, get_target_tensor(pre_real_B, True))
				fake_B = self.G_AtoB.forward(images_A)
				pre_fake_B = self.D_B.forward(fake_B)
				D_B_loss_fake = self.GANloss(pre_fake_B, get_target_tensor(pre_fake_B, False))
				D_B_loss = (D_B_loss_real + D_B_loss_fake) * 0.5

				# Update parameters for discriminators
				D_A_loss.backward()
				D_B_loss.backward()
				self.D_optimizer.step()

			# Finished one epoch, print information, save samples and checkpoints.
			t_end = time.time()
			t_per_epoch = t_end - t_start
			print('Finished epoch: {}!'.format(self.epoch_num))
			print('Time used: {}s'.format(t_per_epoch))	
			# 
			self.save_samples()
			print('Sample saved!')
			# Save the model parameters
			self.save_checkpoint()
			print('checkpoint saved!')
			self.epoch_num += 1


	def test(self):
		test_iter_A = iter(self.test_loader_A)
		test_iter_B = iter(self.test_loader_B)
		# test for A to B
		for i in range(1, len(test_iter_A)):
			image_A = test_iter_A.next()
			image_A = utils.to_var(image_A)
			fake_B = self.G_AtoB(image_A)
			image_A, fake_B = utils.to_data(image_A), utils.to_data(fake_B)

			merged = merge_images( image_A, fake_B, self.opts)
			file_name = 'test_' + self.opts.A + '_to_' + self.opts.B + '_{}.png'.format(i)
			path = os.path.join(self.opts.test_dir, self.opts.A, file_name)
			scipy.misc.imsave(path, merged)

		for i in range(1, len(test_iter_B)):
			image_B = test_iter_B.next()
			image_B = utils.to_var(image_B)
			fake_A = self.G_BtoA(image_B)
			image_B, fake_A = utils.to_data(image_B), utils.to_data(fake_A)

			merged = merge_images( image_B, fake_A, self.opts)
			file_name = 'test_' + self.opts.B + '_to_' + self.opts.A + '_{}.png'.format(i)
			path = os.path.join(self.opts.test_dir, self.opts.B, file_name)
			scipy.misc.imsave(path, merged)

		print('Finished Test!')


def set_requires_grad(nets, requires_grad=False):
	"""
	This function is adapted from https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix
	Description
    ===========
    Set requies_grad=Fasle for all the networks to avoid unnecessary computations
	Parameters:
		nets (network list)   -- a list of networks
		requires_grad (bool)  -- whether the networks require gradients or not
		
	"""
	if not isinstance(nets, list):
		nets = [nets]
	for net in nets:
		if net is not None:
			for param in net.parameters():
				param.requires_grad = requires_grad


def get_target_tensor(prediction, target_is_real):
	"""
	This function is adapted from https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix
	Description
    ===========
    Get the target tensor which has the same shape with prediction
	"""
		if target_is_real:
			target_tensor = torch.tensor(1.0)
		else:
			target_tensor = torch.tensor(0.0)
		if torch.cuda.is_available():
			target_tensor = target_tensor.cuda()
		return target_tensor.expand_as(prediction)

def merge_images(sources, targets, opts):
	"""
	Description
    ===========
    Merge real and generated images
	"""
	_, _, h, w = sources.shape
	row = int(opts.num_sample)
	merged = np.zeros([3, row*h, w*2])
	for idx, (s, t) in enumerate(zip(sources, targets)):
		merged[:, idx*h:(idx+1)*h, 0:w] = s
		merged[:, idx*h:(idx+1)*h, w:2*w] = t
	return merged.transpose(1, 2, 0)

def create_parser():
	"""Creates a parser for command-line arguments.
	"""
	parser = argparse.ArgumentParser()

	# data options
	parser.add_argument('--train_image_size', type=int, default=256, help='The side length N to convert images to NxN.')
	parser.add_argument('--test_image_size', type=int, default=512, help='The side length N to convert images to NxN.')
	parser.add_argument('--num_workers', type=int, default=0, help='The number of threads to use for the DataLoader.')
	parser.add_argument('--A', type=str, default='summer')
	parser.add_argument('--B', type=str, default='winter')

	# training parameters
	parser.add_argument('--train_epochs', type=int, default=100, help='The number of training epochs to run.')
	parser.add_argument('--batch_size', type=int, default=1, help='The number of images in a batch.')
	parser.add_argument('--lr', type=float, default=0.0003, help='The learning rate (default 0.0003)')
	parser.add_argument('--beta1', type=float, default=0.5)
	parser.add_argument('--beta2', type=float, default=0.999)
	parser.add_argument('--lambda_A', type=int , default=5)
	parser.add_argument('--lambda_B', type=int , default=5)
	parser.add_argument('--num_resblocks', type=int , default=6, help='number of resnet blocks used')
	parser.add_argument('--lambda_idt', type=int , default=0.5)
	parser.add_argument('--use_identity_loss', type=bool , default=True)
	# data direction
	parser.add_argument('--image_dir', type=str, default='data')

	# save direction
	parser.add_argument('--sample_dir', type=str, default='samples_cyclegan')
	parser.add_argument('--test_dir', type=str, default='test_results', help='dir to save test results')
	parser.add_argument('--checkpoint_dir', type=str, default='checkpoints_cyclegan')

	# Otehr paramters
	parser.add_argument('--mode', type=str, default='train', help='Control training or testing')
	parser.add_argument('--load', type=str, default=None, help='direction to pretrained model')
	parser.add_argument('--epoch_num', type=int , default=1, help='choose an certain epoch to continue training')
	parser.add_argument('--num_sample', type=int , default=3, help='number of displayed samples while training')


	return parser


def main(opts):
	# Create model
	model = CycleGAN(opts)
	if opts.mode == 'train':
		# Create checkpoint and sample directories
		utils.create_dir(opts.checkpoint_dir)
		utils.create_dir(opts.sample_dir)
		# train
		model.train()
	if opts.mode == 'test':
		# test
		print('Start Test !')
		path_A = os.path.join(opts.test_dir, opts.A)
		utils.create_dir(path_A)
		path_B = os.path.join(opts.test_dir, opts.B)
		utils.create_dir(path_B)
		opts.num_sample = 1
		if opts.load is not None:
			model.test()
		else:
			print('Please provide model load direction !')

if __name__ == '__main__':

    parser = create_parser()
    opts = parser.parse_args()

    utils.print_opts(opts)
    main(opts)

    
