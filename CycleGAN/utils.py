import os
import numpy as np
import torch
from torch.autograd import Variable


def to_var(x):
    """
    Description
    ===========
    Converts numpy to variable.
    """
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


def to_data(image):
    """
    Description
    ===========
    Converts variable to numpy.
    """
    image = image.to('cpu').detach().numpy()
    image = (image + 1) / 2
    image[image < 0] = 0
    image[image > 1] = 1
    return image


def create_dir(directory):
    """
    Description
    ===========
    Creates a directory if it does not already exist.
    """
    if not os.path.exists(directory):
        os.makedirs(directory)

def print_opts(opts):
    """
    Description
    ===========
    Prints the values of all command-line arguments.
    """
    print('=' * 80)
    print('Opts'.center(80))
    print('-' * 80)
    for key in opts.__dict__:
        if opts.__dict__[key]:
            print('{:>30}: {:<30}'.format(key, opts.__dict__[key]).center(80))
    print('=' * 80)