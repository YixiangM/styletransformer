"""
This file contains network models used in CycleGAN
"""
import numpy as np
import torch
from torch import nn
import torch.nn.functional as F


class ResnetBlock(nn.Module):
    """
    Description
    ===========
    Create a resnet block with 2 conv layers

    Input parameters
    ================
    nc    --  number of input and output channels of conv layer
    BatchNorm   --  True: use batch normalization; False: use instance normalization
    use_bias    --  use bias or not
    """

    def __init__(self, nc, BatchNorm, use_bias):
        super(ResnetBlock, self).__init__()

        self.resnet = nn.ModuleList()

        #0. Conv layer
        self.resnet.append(nn.Conv2d(nc, nc, kernel_size=3, stride=1, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.resnet[0].weight.data)

        #1. Norm layer
        if BatchNorm:
            self.resnet.append(nn.BatchNorm2d(nc, nc)) 
            nn.init.normal_(self.resnet[1].weight.data, 1.0, 0.02)
        else:
            self.resnet.append(nn.InstanceNorm2d(nc))
       
        #2. ReLU
        self.resnet.append(nn.ReLU(True))

        #3. Conv layer
        self.resnet.append(nn.Conv2d(nc, nc, kernel_size=3, stride=1, padding=1, bias=use_bias))
        
        #4. Norm layer
        if BatchNorm:
            self.resnet.append(nn.BatchNorm2d(nc, nc))
        else:
            self.resnet.append(nn.InstanceNorm2d(nc))

        # make module list sequential
        self.resnet = nn.Sequential(*self.resnet)

    def forward(self, x):
        # forward with skip connection
        out = x + self.resnet(x)
        return out

class ResnetGenerator(nn.Module):
    """
    Description
    ===========
    Create a resnet generator

    Input parameters
    ================
    BatchNorm       --  True: use batch normalization; False: use instance normalization
    use_bias        --  use bias or not
    num_resblocks   --  number of resnet blocks used
    """
    def __init__(self, BatchNorm, use_bias, num_resblocks):
        super(ResnetGenerator, self).__init__()

        # Encoder part
        #=============
        self.encoder = nn.ModuleList()
        #0 Reflection padding 
        self.encoder.append(nn.ReflectionPad2d(3))
        #1 Conv layer
        self.encoder.append(nn.Conv2d(3, 64, kernel_size=7, padding=0, bias=use_bias))
        nn.init.kaiming_normal_(self.encoder[1].weight.data)  
        #2 Norm layer
        if BatchNorm:
            self.encoder.append(nn.BatchNorm2d(64, 64))
            nn.init.normal_(self.encoder[2].weight.data, 1.0, 0.02)
        else:
            self.encoder.append(nn.InstanceNorm2d(64))     
        #3 ReLU
        self.encoder.append(nn.ReLU(True))
        # start downsampling
        #4 Conv layer
        self.encoder.append(nn.Conv2d(64, 128, kernel_size=3, stride=2, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.encoder[4].weight.data)
        #5 Norm layer
        if BatchNorm:
            self.encoder.append(nn.BatchNorm2d(128, 128))
            nn.init.normal_(self.encoder[5].weight.data, 1.0, 0.02)
        else:
            self.encoder.append(nn.InstanceNorm2d(128))
        #6 ReLU
        self.encoder.append(nn.ReLU(True))
        #7 Conv layer
        self.encoder.append(nn.Conv2d(128, 256, kernel_size=3, stride=2, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.encoder[7].weight.data)
        #8 Norm layer
        if BatchNorm:
            self.encoder.append(nn.BatchNorm2d(256, 256))
            nn.init.normal_(self.encoder[8].weight.data, 1.0, 0.02)
        else:
            self.encoder.append(nn.InstanceNorm2d(256))
        #9 ReLU
        self.encoder.append(nn.ReLU(True))
        # make module list sequential
        self.encoder = nn.Sequential(*self.encoder)

        # Resblocks
        #==========
        self.resnetblocks = nn.ModuleList()
        # create resnet blocks
        for i in range(num_resblocks):
            self.resnetblocks.append(ResnetBlock(256, BatchNorm, use_bias))

        self.resnetblocks = nn.Sequential(*self.resnetblocks)

        #Decoder
        #=======
        self.decoder = nn.ModuleList()
        #0 Deconv layer
        self.decoder.append(nn.ConvTranspose2d(256, 128, kernel_size=3, stride=2, padding=1, output_padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.decoder[0].weight.data)
        #1 Norm layer
        if BatchNorm:
            self.decoder.append(nn.BatchNorm2d(128, 128))
            nn.init.normal_(self.decoder[1].weight.data, 1.0, 0.02)
        else:
            self.decoder.append(nn.InstanceNorm2d(128))
        #2 ReLU
        self.decoder.append(nn.ReLU(True))
        #3 Deconv layer
        self.decoder.append(nn.ConvTranspose2d(128, 64, kernel_size=3, stride=2, padding=1, output_padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.decoder[3].weight.data)
        #4 Norm layer
        if BatchNorm:
            self.decoder.append(nn.BatchNorm2d(64, 64))
            nn.init.normal_(self.decoder[4].weight.data, 1.0, 0.02)
        else:
            self.decoder.append(nn.InstanceNorm2d(64))
        #5 ReLU
        self.decoder.append(nn.ReLU(True))
        #6 Reflect padding
        self.decoder.append(nn.ReflectionPad2d(3))
        #7 Conv layer
        self.decoder.append(nn.Conv2d(64, 3, kernel_size=7, padding=0, bias=use_bias))
        #8 Tanh layer
        self.decoder.append(nn.Tanh())
        # make module list sequential
        self.decoder = nn.Sequential(*self.decoder)
    def forward(self, x):
        out = self.encoder(x)
        out = self.resnetblocks(out)
        out = self.decoder(out)
        return out


class Discriminator(nn.Module):
    """
    Description
    ===========
    Create a discriminator with 6 conv layers

    Input parameters
    ================
    BatchNorm       --  True: use batch normalization; False: use instance normalization
    use_bias        --  use bias or not
    """
    def __init__(self, BatchNorm, use_bias):
        super(Discriminator, self).__init__()
        
        self.model = nn.ModuleList()
        #1============
        #0 Conv layer
        self.model.append(nn.Conv2d(3, 64, kernel_size=4, stride=2, padding=1))
        nn.init.kaiming_normal_(self.model[0].weight.data)
        #1 LeakyReLU
        self.model.append(nn.LeakyReLU(0.2, True))
        #2============
        #2 Conv layer
        self.model.append(nn.Conv2d(64, 128, kernel_size=4, stride=2, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.model[2].weight.data)
        #3 Norm layer
        if BatchNorm:
            self.model.append(nn.BatchNorm2d(128))
            nn.init.normal_(self.model[3].weight.data, 1.0, 0.02)
        else:
            self.model.append(nn.InstanceNorm2d(128))
        #4 Leaky ReLU
        self.model.append(nn.LeakyReLU(0.2, True))
        #3============
        #5 Conv layer
        self.model.append(nn.Conv2d(128, 256, kernel_size=4, stride=2, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.model[5].weight.data)
        #6 Norm layer
        if BatchNorm:
            self.model.append(nn.BatchNorm2d(256))
            nn.init.normal_(self.model[6].weight.data, 1.0, 0.02)
        else:
            self.model.append(nn.InstanceNorm2d(256))
        #7 Leaky ReLU
        self.model.append(nn.LeakyReLU(0.2, True))
        #4============
        #8 Conv layer
        self.model.append(nn.Conv2d(256, 512, kernel_size=4, stride=2, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.model[8].weight.data)
        #9 Norm layer
        if BatchNorm:
            self.model.append(nn.BatchNorm2d(512))
            nn.init.normal_(self.model[9].weight.data, 1.0, 0.02)
        else:
            self.model.append(nn.InstanceNorm2d(512))
        #10 Leaky ReLU
        self.model.append(nn.LeakyReLU(0.2, True))
        #5=============
        #11 Conv layer
        self.model.append(nn.Conv2d(512, 512, kernel_size=4, stride=1, padding=1, bias=use_bias))
        nn.init.kaiming_normal_(self.model[11].weight.data)
        #12 Norm layer
        if BatchNorm:
            self.model.append(nn.BatchNorm2d(512))
            nn.init.normal_(self.model[12].weight.data, 1.0, 0.02)
        else:
            self.model.append(nn.InstanceNorm2d(512))
        #13 Leaky ReLU
        self.model.append(nn.LeakyReLU(0.2, True))
        #6=============
        #14 Conv layer
        self.model.append(nn.Conv2d(512, 1, kernel_size=4, stride=1, padding=1))
        # make module list sequential
        self.model = nn.Sequential(*self.model)

    def forward(self, x):
        out = self.model(x)
        return out
