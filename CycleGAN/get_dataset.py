import os
import torch.utils.data as td
import torchvision as tv
from PIL import Image


class StyleDataset(td.Dataset):

    def __init__(self, opts, mode, image_type):
        super(StyleDataset, self).__init__()
        self.mode = mode
        if self.mode == 'train':
            self.image_size = opts.train_image_size
            self.images_dir = os.path.join(opts.image_dir, 'train{}'.format(image_type))
            self.files = os.listdir(self.images_dir)
        else:
            self.image_size = opts.test_image_size
            self.images_dir = os.path.join(opts.image_dir, 'test{}'.format(image_type))
            self.files = os.listdir(self.images_dir)

    def __len__(self):
        return len(self.files)

    def __repr__(self):
        return "StyleDataset(mode={}, image_size={})". \
            format(self.mode, self.image_size)

    def __getitem__(self, idx):
        img_path = os.path.join(self.images_dir, self.files[idx])
        img = Image.open(img_path).convert('RGB')
        transform = tv.transforms.Compose([
            tv.transforms.Resize(self.image_size),
            tv.transforms.ToTensor(),
            tv.transforms.Normalize((1/2, 1/2, 1/2), (1/2, 1/2, 1/2)),
            ])
        img = transform(img)
        
        return img