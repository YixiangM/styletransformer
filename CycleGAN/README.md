## Requirements

Nothing special

## Code organization

demo.ipynb	     —	run a demo of our code (results in section 4.2) 

train.ipynb		  —	train our model

test.ipynb		    —	test our model using all test images

model.py 		    —	contains architecture of our discriminator and generator

cycleGAN.py	  —  includes training and testing

get_dataset.py	—	get dataset

utils.py 			   —	useful tools

## Run with command line

### Start trian with default parameters

For example, style transfer between pantings of Monet and landscape photographs

```shell
$ python cycleGAN.py --A landscape --B monet --checkpoint_dir model/model_monet
```

If you would like to try paintings from other painters, we have dataset of paintings of Vincent van Gogh and Paul Cezanne.

Replace all 'monet' with 'van' for Vincent van Gogh.

Replace all 'monet' with 'cezanne' for Paul Cezanne.

### Continue training 

For example, style transfer between pantings of Monet and landscape photographs and continue with epoch number 100

```shell
$ python cycleGAN.py --A landscape --B monet --checkpoint_dir model/model_monet --load model/model_monet --epoch_num 100 --lr 0.0001
```

### Test

For example, style transfer between pantings of Monet and landscape photographs, test models which trained 100 epochs.

```shell
$ python cycleGAN.py --A landscape --B monet --mode test --load model/model_monet --epoch_num 78 --test_image_size 1024 --num_sample 1
```

