## Requirements

Install package 'imageio' as follow:

```shell
$ pip install --user imageio
```



## Code organization

demo.ipynb	  —	run a demo of our code (results in section 4.1) 

myim.py            —	load preprocess images from url and show image functions 

model.py           —	module contains cnn model, style loss and content loss

