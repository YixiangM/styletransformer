import matplotlib.pyplot as plt
import torchvision.transforms as transforms
import torch
from io import BytesIO
from PIL import Image
import requests

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

imsize = 1024 if torch.cuda.is_available() else 128  

loader = transforms.Compose([
    transforms.Resize((imsize,imsize)),  # scale imported image
    transforms.ToTensor()])  # transform it into a torch tensor

unloader = transforms.ToPILImage()  # reconvert into PIL image

def myimshow(image,title):
    
    image = image.cpu().clone()  
    image = image.squeeze(0)      
    image = unloader(image)
    plt.imshow(image)
    plt.axis('off')
    plt.title(title)
    plt.pause(0.001) 
    image.save(title+".png")

def image_loader(url):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    # fake batch dimension required to fit network's input dimensions
    image = loader(img).unsqueeze(0)
    return image.to(device, torch.float)
