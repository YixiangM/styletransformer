## Description

This is project style transfer developed by team StyleTransformer composed of Yixiang Mao, Shiwei Jin and Yujie Zhou. We implemented neural style transfer and cycle GAN.



## Files

There are two folders: 

​	Neural_Style_Transfer	— things related to Neural Style Transfer

​	CycleGAN						 — things related to Cycle GAN	

Besides, there are two README files in both folders.